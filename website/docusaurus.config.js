/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'My Site',
  tagline: 'The tagline of my site',
  url: 'https://kmccullochcop.gitlab.io',
  baseUrl: '/designfrolics/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'LINCS SCALE', // Usually your GitHub org/user name.
  projectName: 'designfrolics', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Design Frolics',
      logo: {
        alt: 'Design Frolic',
        src: 'img/designfrolicLogo.jpg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Past Frolics',
          position: 'left',
        },
      ],
    },
    footer: {
      style: 'light',
      links: [
        {
          title: 'Contact Us',
          items: [
            {
              label: 'Kim-Martin.ca',
              to: 'https://www.kim-martin.ca/',
            },
            // {
            //   label: 'LINCS',
            //   to: '  https://lincsproject.ca/',
            // },
            {
              label: 'LINCS GitLab',
              to: 'https://gitlab.com/calincs',
            },
          ],
        },
        {
          title: 'Frolics',
          items: [
            {
              label: 'All Past Frolics',
              to: '/docs/',
            },
          ],
        },
      ],
      copyright: `Created by Kim Martin, Rashmeet Kaur, and Kathleen McCulloch-Cop. Made possible by SCALE and LINCS.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
